// Copyright Dave Kahler. Do not copy without permission.
var TrackPackage_functionLib =
{
 regexDefaults: '"USPS","((94001|92055|94073|93033|92701|92088|92021)(\\d{15}))|((9400 1|9205 5|9407 3|9303 3|9270 1|9208 8|9202 1)\\d{3} \\d{4} \\d{4} \\d{4} \\d{2})|((82)\\d{8})|((82) \\d{3} \\d{3} \\d{2})|((EC|EA|CP|RA|EL|LZ)(\\d{9})US)|((EC|EA|CP|RA|EL|LZ)( \\d{3} \\d{3} \\d{3}) US)";"UPS","(1Z\\w{16})|(\\d{12})|((T|W|H)\\d{10})|(\\d{9})";"FedEx","((\\d{22})|(\\d{20})|(\\d{15})|(\\d{14})|(\\d{12}))"',
 URLDefaults: '"USPS","https://tools.usps.com/go/TrackConfirmAction_input?tLabels=","";"UPS","http://wwwapps.ups.com/etracking/tracking.cgi?InquiryNumber1=","&TypeOfInquiryNumber=T&AcceptUPSLicenseAgreement=yes&submit=Track";"FedEx","https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=","&locale=en_US&cntry_code=us"',
 _RemoveSpaces: function(s)
 {
  let r = '';
  s = s.toString();
  for (let i = 0; i < s.length; i++)
  {
   if ((s.charAt(i) !== ' ') && (s.charAt(i) !== '.') && (s.charAt(i) !== ','))
    r += s.charAt(i);
  }
  return r;
 },
 OnBlur: function(event)
 {
  let b = gBrowser.getNotificationBox();
  let a = b.allNotifications;
  for (let i = 0; i < a.length; i++)
  {
   let e = a[i];
   if (e.label.match('Track Package') || e.label.match('Tracked Package'))
    e.close();
  }
 },
 _searchSelected: function()
 {
  let k = document.popupNode;
  let s = '';
  if ((k instanceof HTMLTextAreaElement) || (k instanceof HTMLInputElement && k.type === 'text'))
  {
   s = k.value.substring(k.selectionStart, k.selectionEnd);
  } 
  else
  {
   let w = new XPCNativeWrapper(document.commandDispatcher.focusedWindow, 'document', 'getSelection()');
   s = w.getSelection().toString();
  }
  let r = s;
  r = r.toString();
  r = r.replace( /^\s+/, '' );
  r = r.replace(/(\n|\r|\t)+/g, ' ');
  r = r.replace(/\s+$/,'');
  return r;
 },
 GetTabSetting: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('NewTab'))
   return p.getBoolPref('NewTab');
  if (p.prefHasUserValue('tpNewTab'))
   return p.getBoolPref('tpNewTab');
  return true;
 },
 GetSmartSenseSetting: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('SmartSense'))
   return p.getBoolPref('SmartSense');
  if (p.prefHasUserValue('tpSmartSense'))
   return p.getBoolPref('tpSmartSense');
  return true;
 },
 _GetNotificationsSetting: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('Notifications'))
   return p.getBoolPref('Notifications');
  if (p.prefHasUserValue('tpNotifications'))
   return p.getBoolPref('tpNotifications');
  return true;
 },
 _GetPrivateBrowsingSetting: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('PrivateBrowsing'))
   return p.getBoolPref('PrivateBrowsing');
  if (p.prefHasUserValue('tpPrivateBrowsing'))
   return p.getBoolPref('tpPrivateBrowsing');
  return true;
 },
 GetGMapsSetting: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('EnableGMaps'))
   return p.getBoolPref('EnableGMaps');
  if (p.prefHasUserValue('tpEnableGMaps'))
   return p.getBoolPref('tpEnableGMaps');
  return false;
 },
 _AllIndicesOf: function(c, s)
 {
  let a = [];
  for (let i = 0; i < s.length; i++)
  {
   if (s[i] === c)
   {
    a[a.length] = i;
   }
  }
  return a;
 },
 _GetHistoryArray: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  let s = '';
  if (p.prefHasUserValue('TrackingHistory'))
   s = p.getCharPref('TrackingHistory');
  else if (p.prefHasUserValue('tpTrackingHistory'))
   s = p.getCharPref('tpTrackingHistory');
  if (s.length < 1)
   return [];
  let h = s.split(';');
  if (h.length < 1)
   return [];
  let a = [];
  for (let i = 0; i < h.length; i++)
  {
   let v = h[i].split(',');
   a[i] = [];
   a[i].Carrier = v[0];
   a[i].TrackingNumber = v[1];
   a[i].Date = v[2];
   a[i].Notes = v[3];
  }
  return a;
 },
 ExtractQuotedStrings: function(s)
 {
  let d = TrackPackage_functionLib._AllIndicesOf('"', s);
  if (d.length % 2 !== 0)
  {
   alert('Malformed preference! Contact webmaster@realityripple.com');
   return '';
  }
  let a = [];
  for (let i = 0; i < d.length; i += 2)
  {
   let q = s.substring(d[i] + 1, d[i + 1]);
   a[a.length] = q;
  }
  return a;
 },
 GetRegexURLArray: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  let b = TrackPackage_functionLib.regexDefaults.split(';');
  if (p.prefHasUserValue('Regex'))
   b = p.getCharPref('Regex').split(';');
  else if (p.prefHasUserValue('tpRegex'))
   b = p.getCharPref('tpRegex').split(';');
  let r = [];
  for (let i = 0; i < b.length; i++)
  {
   let g = TrackPackage_functionLib.ExtractQuotedStrings(b[i]);
   let x = r.length;
   r[x] = [];
   r[x][0] = g[0];
   r[x][1] = g[1];
  }
  let a =TrackPackage_functionLib.URLDefaults.split(';');
  if (p.prefHasUserValue('URL'))
   a = p.getCharPref('URL').split(';');
  else if (p.prefHasUserValue('tpURL'))
   a = p.getCharPref('tpURL').split(';');
  for (let i = 0; i < a.length; i++)
  {
   let o = TrackPackage_functionLib.ExtractQuotedStrings(a[i]);
   let m = false;
   for (let j = 0; j < r.length; j++)
   {
    if (r[j][0].toLowerCase() === o[0].toLowerCase())
    {
     r[j][2] = o[1];
     r[j][3] = o[2];
     m = true;
    }
   }
   if (m)
    continue;
   let d = r.length;
   r[d] = [];
   r[d][0] = o[0];
   r[d][1] = '';
   r[d][2] = o[1];
   r[d][3] = o[2];
  }
  return r;
 },
 VerifyPackageFedEx: function(s)
 {
  if (s.length === 12)
  {
   let v = 0;
   for (let i = 0; i < s.length - 1; i++)
   {
    let c = parseInt(s.substring(i, i + 1), 10);
    let m = 0;
    switch (i % 3)
    {
     case 0:
      m = c * 3;
      break;
     case 1:
      m = c;
      break;
     case 2:
      m = c * 7;
      break;
    }
    v += m;
   }
   let d = v % 11;
   if (d === 10)
    d = 0;
   let k = parseInt(s.substring(s.length - 1, s.length), 10);
   if (k === d)
    return true;
   return false;
  }
  if (s.length === 15)
  {
   let m1 = 0;
   let m2 = 0;
   for (let i = s.length - 2; i >= 0; i--)
   {
    let f = parseInt(s.substring(i, i + 1), 10);
    switch (i % 2)
    {
     case 0:
      m2 += f;
      break;
     case 1:
      m1 += f;
    }
   }
   m1 *= 3;
   let t = m1 + m2;
   let n = Math.ceil(t / 10) * 10;
   let d2 = n - t;
   let k2 = parseInt(s.substring(s.length - 1, s.length), 10);
   if (k2 === d2)
    return true;
   return false;
  }
  return true;
 },
 VerifyPackageCarrier: function(c, s)
 {
  switch (c.toLowerCase())
  {
   case 'fedex':
    return TrackPackage_functionLib.VerifyPackageFedEx(s);
  }
  return true;
 },
 GetPackageCarrier: function(s)
 {
  let a = TrackPackage_functionLib._GetHistoryArray();
  for (let i = 0; i < a.length; i++)
  {
   if (a[i].TrackingNumber === s)
    return a[i].Carrier;
  }
  let x = TrackPackage_functionLib.GetRegexURLArray();
  let c;
  for (i = 0; i < x.length; i++)
  {
   if (x[i][1] === '')
    continue;
   let r = new RegExp(x[i][1], 'gi');
   if (r.test(s))
   {
    if (!TrackPackage_functionLib.VerifyPackageCarrier(x[i][0], s))
     continue;
    c = x[i][0];
    break;
   }
  }
  return c;
 },
 _IsInPrivateBrowsingMode: function()
 {
  let s;
  try
  {
   s = Components.classes['@mozilla.org/privatebrowsing;1'].getService(Components.interfaces.nsIPrivateBrowsingService);
   if (!('privateBrowsingEnabled' in s))
    s = undefined;
  }
  catch(e) {}
  let u;
  try
  {
   u = Components.utils.import('resource://gre/modules/PrivateBrowsingUtils.jsm', {}).PrivateBrowsingUtils;
  }
  catch(e) {}
  let g = !!s;
  let p = !g && !!u;
  if (p)
   return u.isWindowPrivate(document.commandDispatcher.focusedWindow);
  else if (g)
   return true;
 },
 GetTrackingString: function()
 {
  return TrackPackage_functionLib._RemoveSpaces(TrackPackage_functionLib._searchSelected());
 },
 RebuildDropdown: function()
 {
  let b = document.getElementById('tpButtonMenu');
  let p = document.getElementById('tpButtonMenuPopup');
  if (typeof(b) !== 'undefined' && b !== null)
  {
   b.className = 'toolbarbutton-1';
   while (p.firstChild)
   {
    p.removeChild(p.firstChild);
   }
   let r = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
   let m = 5;
   if (r.prefHasUserValue('MaxDropdownItems'))
    m = r.getIntPref('MaxDropdownItems');
   else if (r.prefHasUserValue('tpMaxDropdownItems'))
    m = r.getIntPref('tpMaxDropdownItems');
   let a = TrackPackage_functionLib._GetHistoryArray();
   if (a.length === 0)
   {
    b.type = '';
    return;
   }
   b.type = 'menu-button';
   for (let i = 0; i < a.length; i++)
   {
    let l = a[i].Carrier + ': ' + a[i].TrackingNumber;
    let u = true;
    if (typeof a[i].Notes === 'undefined' || a[i].Notes === null || a[i].Notes === '')
     u = false;
    else
    {
     for (let j = 0; j < a.length; j++)
     {
      if (j === i)
       continue;
      if (typeof a[j].Notes === 'undefined' || a[j].Notes === null || a[j].Notes === '')
       continue;
      if (a[i].Notes.toLowerCase() === a[j].Notes.toLowerCase())
      {
       u = false;
       break;
      }
     }
    }
    if (u)
     l = a[i].Notes;
    let t = document.createElement('menuitem');
    t.setAttribute('label', l);
    t.historyInfo = a[i];
    p.appendChild(t);
    if (i === m - 1)
     break;
   }
  }
 },
 OpenHistory: function()
 {
  let w = window.open('chrome://trackpackage/content/history.xul', 'trackinghistory', 'chrome=yes,resizable=yes,centerscreen=yes,scrollbars=yes,width=720,height=600,');
  w.focus();
  w.onunload = TrackPackage_functionLib.RebuildDropdown;
 },
 _AddToHistory: function(c, g)
 {
  if (TrackPackage_functionLib._GetPrivateBrowsingSetting() && TrackPackage_functionLib._IsInPrivateBrowsingMode())
   return;
  let n = new Date();
  let d = (n.getMonth()+1) + '/' + n.getDate() + '/' + n.getFullYear();
  let r;
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  let s = '';
  if (p.prefHasUserValue('TrackingHistory'))
   s = p.getCharPref('TrackingHistory');
  else if (p.prefHasUserValue('tpTrackingHistory'))
   s = p.getCharPref('tpTrackingHistory');
  let m = 25;
  if (p.prefHasUserValue('MaxNumbers'))
   m = p.getIntPref('MaxNumbers');
  else if (p.prefHasUserValue('tpMaxNumbers'))
   m = p.getIntPref('tpMaxNumbers');
  let e = s.split(';').length;
  if (e === m)
  {
   let x = s.lastIndexOf(';');
   s = s.substring(0, x);
  }
  let t = s.indexOf(g + ',');
  if (t !== -1)
  {
   for (let i = t - 2; i >= 0; i--)
   {
    if (s[i] === ';')
     break;
   }
   for (let j = t; j < s.length; j++)
   {
    if (s[j] === ',')
     break;
   }
   i++;
   r = s.replace(s.substr(i, j - i), c + ',' + g);
  }
  else
  {
   if (s.length > 0)
    r = c + ',' + g + ',' + d + ';' + s;
   else
    r = c + ',' + g + ',' + d;
  }
  p.setCharPref('TrackingHistory', r);
  TrackPackage_functionLib.RebuildDropdown();
 },
 GetPackageURL: function(c, s, a)
 {
  if (s === '')
   return('');
  if (typeof c === 'undefined' || c === null)
   c = '';
  let u = '';
  let x = TrackPackage_functionLib.GetRegexURLArray();
  for (let i = 0; i < x.length; i++)
  {
   if (x[i][1] === '' && c.length === 0)
    continue;
   let regex = new RegExp(x[i][1],'gi');
   if ((c.length === 0 && regex.test(s)) || (c.toLowerCase() === x[i][0].toLowerCase()))
   {
    c = x[i][0];
    u = x[i][2] + s + x[i][3];
    break;
   }
  }
  if (a)
   TrackPackage_functionLib._AddToHistory(c, s);
  return u;
 },
 ButtonCallback: function(n, b)
 {
  let u = TrackPackage_functionLib.GetRegexURLArray();
  for (let i = 0;i < u.length; i++)
  {
   let c = u[i][0];
   if (c === b.label)
   {
    let t = c + ': ' + n.value;
    TrackPackage_functionLib.OpenPackageWindow(TrackPackage_functionLib.GetPackageURL(c, n.value, true), false, false, t);
   }
  }
 },
 _ShowNotificationBox: function(f)
 {
  let n = gBrowser.getNotificationBox();
  let r = n.allNotifications;
  for (let i = 0; i < r.length; i++)
  {
   let d = r[i];
   if (d.label.match('Track Package') || d.label.match('Tracked Package'))
    d.close();
  }
  let b = [];
  let a = TrackPackage_functionLib.GetRegexURLArray();
  for (let i = 0; i < a.length; i++)
  {
   let c = a[i][0];
   b[i] = 
   {
    label: c,
    accessKey: '',
    callback: TrackPackage_functionLib.ButtonCallback
   };
  }
  if (TrackPackage_functionLib._GetNotificationsSetting() || f)
  {
   let o = TrackPackage_functionLib.GetTrackingString();
   let t = 'Track Package - Choose Carrier';
   if (!f)
    t = 'Tracked Package via ' + TrackPackage_functionLib.GetPackageCarrier(o) + '. Fix Carrier?';
   n.appendNotification(t, o, 'chrome://trackpackage/skin/logo.png', n.PRIORITY_INFO_HIGH, b);
  }
 },
 OpenPackageWindow: function(u, f, c, t)
 {
  if (u !== '')
  {
   if (!TrackPackage_functionLib.GetTabSetting() && !f)
    window.open(u);
   else
   {
    if (!f)
     openNewTabWith(u, null, null, null, false, null);
    else
    {
     window.opener.gBrowser.loadOneTab(u,
     {
      referrerURI: null,
      charset: null,
      postData: null,
      inBackground: true,
      allowThirdPartyFixup: false,
      relatedToCurrent: false,
      isUTF8: true
     });
    }
    if (typeof getBrowser === 'function')
     getBrowser().tabContainer.addEventListener('TabSelect', TrackPackage_functionLib.OnBlur, false);
   }
   if (c)
    TrackPackage_functionLib._ShowNotificationBox(false);
  }
  else
   TrackPackage_functionLib._ShowNotificationBox(true);
 },
 HistoryOpenMap: function(c, s, t)
 {
  let u = 'http://www.packagemapping.com?action=track&shipper=' + c + '&tracknum=' + s;
  if (c.length)
   TrackPackage_functionLib.OpenPackageWindow(u, t, false);
 },
 TrackPackage: function()
 {
  if (TrackPackage_functionLib.GetSmartSenseSetting())
  {
   let s = TrackPackage_functionLib.GetTrackingString();
   let c = TrackPackage_functionLib.GetPackageCarrier(s);
   let t = c + ': ' + s;
   TrackPackage_functionLib.OpenPackageWindow(TrackPackage_functionLib.GetPackageURL(c, s, true), false, true, t);
  }
  else
   TrackPackage_functionLib.OpenPackageWindow('', false, true, '');
 },
 TrackGoogleMaps: function()
 {
  let s = TrackPackage_functionLib.GetTrackingString();
  let o = TrackPackage_functionLib.GetPackageCarrier(s);
  let c = o.toLowerCase();
  let u = 'http://www.packagemapping.com?action=track&shipper=' + c + '&tracknum=' + s;
  if (c.length)
  {
   TrackPackage_functionLib.OpenPackageWindow(u, false, false, '');
   TrackPackage_functionLib._AddToHistory(o, s);
  }
 }
};
