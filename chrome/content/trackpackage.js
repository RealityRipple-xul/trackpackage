// Copyright Dave Kahler. Do not copy without permission.
var TrackPackage_overlay =
{
 _branch: null,
 observe: function(s, t, d)
 {
  if (t === 'nsPref:changed' && d === 'tpMaxDropdownItems')
   TrackPackage_functionLib.RebuildDropdown();
  if (d !== 'item-uninstalled')
   return;
  s = s.QueryInterface(Components.interfaces.nsIUpdateItem);
  if (s.name !== 'Track Package')
   return;
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  p.deleteBranch('');
 },
 register: function()
 {
  let o = Components.classes['@mozilla.org/observer-service;1'].getService(Components.interfaces.nsIObserverService);
  o.addObserver(this, 'em-action-requested', false);
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService);
  TrackPackage_overlay._branch = p.getBranch('extensions.trackpackage.');
  TrackPackage_overlay._branch.QueryInterface(Components.interfaces.nsIPrefBranch);
  TrackPackage_overlay._branch.addObserver('', this, false);
 },
 deregister: function()
 {
  let o = Components.classes['@mozilla.org/observer-service;1'].getService(Components.interfaces.nsIObserverService);
  o.removeObserver(this,'em-action-requested');
 },
 Unload: function()
 {
  TrackPackage_overlay.deregister();
 },
 Hide: function()
 {
  let s = TrackPackage_functionLib.GetSmartSenseSetting();
  let t = TrackPackage_functionLib.GetTrackingString();
  let c = TrackPackage_functionLib.GetPackageCarrier(t);
  let h = (TrackPackage_functionLib.GetPackageURL(c, t, false) !== '' || !s);
  let i = document.getElementById('trackpackage');
  let g = document.getElementById('trackgmaps');
  let e = document.getElementById('trackpackage_explicitmenu');
  if (i)
   i.hidden = !h && s;
  if (g)
   g.hidden = !h && s;
  if (e)
   e.hidden = true;
 },
 Init: function()
 {
  TrackPackage_overlay.register();
  let p = TrackPackage_overlay.Hide;
  document.getElementById('contentAreaContextMenu').addEventListener('popupshowing', p, false);
  if (!TrackPackage_functionLib.GetGMapsSetting())
   return;
  let m = document.createElement('menuitem');
  m.setAttribute('id','trackgmaps');
  m.setAttribute('label','Track with Google Maps');
  m.setAttribute('oncommand','TrackPackage_functionLib.TrackGoogleMaps();');
  if (document.getElementById('contentAreaContextMenu') === null)
   return;
  let c = document.getElementById('contentAreaContextMenu');
  let a = c.querySelector('#trackpackage_explicitmenu');
  if (a.nextSibling === null)
   c.appendChild(m);
  else
   c.insertBefore(m, a.nextSibling);
 },
 ButtonMenuPressed: function(t)
 {
  if (t.id === 'tpButtonMenu')
  {
   let o = t.firstChild;
   while (o)
   {
    if (typeof o.historyInfo !== 'undefined' && o.historyInfo !== null)
    {
     t = o;
     break;
    }
    o = o.nextSibling;
   }
  }
  if (typeof t.historyInfo !== 'undefined' && t.historyInfo !== null)
  {
   let c = t.historyInfo.Carrier;
   let n = t.historyInfo.TrackingNumber;
   let s = c + ': ' + n;
   TrackPackage_functionLib.OpenPackageWindow(TrackPackage_functionLib.GetPackageURL(c, n, false), false, false, s);
  }
  else
   TrackPackage_functionLib.OpenHistory();
 },
 ButtonMenuLoaded: function()
 {
  if (typeof TrackPackage_functionLib !== 'undefined' && TrackPackage_functionLib !== null)
   TrackPackage_functionLib.RebuildDropdown();
 }
};
window.addEventListener('load', TrackPackage_overlay.Init, false);
window.addEventListener('unload', TrackPackage_overlay.Unload, false);
