// Copyright Dave Kahler. Do not copy without permission.
var TrackPackage_history =
{
 _BuildHistoryArray: function(historyString)
 {
  return(historyString.split(';'));
 },
 HistoryTrack: function()
 {
  let l = document.getElementById('historyListbox').selectedItems;
  for (let i = 0; i < l.length; i++)
  {
   let n = l[i].childNodes;
   let c = n.item(0).getAttribute('label');
   let s = n.item(1).getAttribute('label');
   let u = TrackPackage_functionLib.GetPackageURL(c, s, false);
   if (u === '')
    continue;
   if (l.length > 1)
   {
    TrackPackage_functionLib.OpenPackageWindow(u, true, false);
    continue;
   }
   if (!TrackPackage_functionLib.GetTabSetting())
    TrackPackage_functionLib.OpenPackageWindow(u, false, false);
   else
    TrackPackage_functionLib.OpenPackageWindow(u, true, false);
  }
 },
 HistoryDelete: function(e)
 {
  let l = document.getElementById('historyListbox');
  if (l.currentIndex === -1)
   return;
  let n = l.currentItem.childNodes;
  let c = n.item(0).getAttribute('label');
  let s = n.item(1).getAttribute('label');
  let t = n.item(3).value;
  let v = '';
  if (t === '')
   v = c + ' ' + s;
  else
   v = t + ' [' + c + ': ' + s + ']';
  let p = 'Are you sure you want to delete this Tracking Number?';
  if (v !== '')
   p = 'Are you sure you want to delete ' + v + '?';
  if (!confirm(p))
   return;
  l.removeItemAt(l.currentIndex);
  TrackPackage_history.onCloseHistory();
 },
 _PopulateListBox: function(h)
 {
  let l = document.getElementById('historyListbox');
  let m = l.getRowCount();
  for (let i = 0; i < m; i++)
  {
   l.removeItemAt(0);
  }
  for (let i = 0; i < h.length; i++)
  {
   let a = h[i].split(',');
   let r = document.createElement('listitem');
   r.setAttribute('allowevents',true);
   let cC = document.createElement('menulist');
   cC = document.createElement('menulist');
   let mC = document.createElement('menupopup');
   cC.appendChild(mC);
   let u = TrackPackage_functionLib.GetRegexURLArray();
   for (let i = 0; i < u.length; i++)
   {
    if (u[i][1] === '')
     continue;
    let sC = u[i][0];
    let mI = document.createElement('menuitem');
    mI.setAttribute('label', sC);
    if (sC === a[0])
     mI.setAttribute('selected', true);
    mC.appendChild(mI);
   }
   r.appendChild(cC);
   let cN = document.createElement('listcell');
   if (a.length < 1 || typeof a[1] !== 'string' || a[1].length < 1)
    continue;
   cN.setAttribute('label', a[1]);
   r.appendChild(cN);
   cN.addEventListener('dblclick', TrackPackage_history.HistoryTrack, true);
   let cD = document.createElement('listcell');
   if (a.length < 2 || typeof a[2] !== 'string' || a[2].length < 1)
   {
    let d = new Date();
    cD.setAttribute('label', (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear());
   }
   else
    cD.setAttribute('label', a[2]);
   r.appendChild(cD);
   cD.addEventListener('dblclick', TrackPackage_history.HistoryTrack, true);
   let cT = document.createElement('textbox');
   if ( a.length < 3 || typeof a[3] !== 'string' || a[3].length < 1)
    cT.setAttribute('value', '');
   else
    cT.setAttribute('value', a[3]);
   cT.setAttribute('clickSelectsAll', true);
   r.appendChild(cT);
   let cB = document.createElement('button');
   cB.setAttribute('label', 'X');
   cB.addEventListener('click', TrackPackage_history.HistoryDelete, true);
   r.appendChild(cB);
   l.appendChild(r);
  }
 },
 HistoryInit: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  let h = '';
  if (p.prefHasUserValue('TrackingHistory'))
   h = p.getCharPref('TrackingHistory');
  else if (p.prefHasUserValue('tpTrackingHistory'))
   h = p.getCharPref('tpTrackingHistory');
  let a = TrackPackage_history._BuildHistoryArray(h);
  TrackPackage_history._PopulateListBox(a);
  if (!TrackPackage_functionLib.GetGMapsSetting())
   return;
  if (document.getElementById('mapButton') !== null)
   return;
  let m = document.createElement('button');
  m.setAttribute('id','mapButton');
  m.setAttribute('label','Map Selected');
  m.setAttribute('oncommand','TrackPackage_history.HistoryMapTrack();');
  m.setAttribute('style','width: 120px; height: 30px;');
  if (document.getElementById('buttonhbox'))
   document.getElementById('buttonhbox').appendChild(m);
  let c = document.createElement('menuitem');
  c.setAttribute('label', 'Map Selected');
  c.setAttribute('oncommand','TrackPackage_history.HistoryMapTrack();');
  if (document.getElementById('historyMenu'))
   document.getElementById('historyMenu').insertBefore(c, document.getElementById('historyMenuSpace'));
 },
 onResizeHistory: function(e)
 {
  let l = document.getElementById('historyListLabel');
  if (l.flex === 1)
   l.flex = 2;
  else if (l.flex === 2)
   l.flex = 1;
 },
 TextUpdate: function(e)
 {
  let l = document.getElementById('historyListbox');
  if (l.selectedIndex === -1)
   return false;
  let c = document.getElementById('historyMenuCopy');
  let d = document.getElementById('historyMenuDel');
  if (l.selectedItems.length === 1)
  {
   c.label = 'Copy Tracking Number';
   d.label = 'Delete Tracking Number';
  }
  else
  {
   c.label = 'Copy Tracking Numbers';
   d.label = 'Delete Tracking Numbers';
  }
  return true;
 },
 HistoryKeyDown: function(e)
 {
  if (e.keyCode !== 46)
   return;
  let l = document.getElementById('historyListbox');
  if (l.selectedIndex === -1)
   return;
  let p = 'Are you sure you want to delete the selected Tracking Numbers?';
  if (l.selectedItems.length === 1)
  {
   let k = l.selectedItem.childNodes;
   let c = k.item(0).getAttribute('label');
   let n = k.item(1).getAttribute('label');
   let t = k.item(3).value;
   let s = '';
   if (t === '')
    s = c + ' ' + n;
   else
    s = t + ' [' + c + ': ' + n + ']';
   p = 'Are you sure you want to delete the selected Tracking Number?';
   if (s !== '')
    p = 'Are you sure you want to delete ' + s + '?';
  }
  if (!confirm(p))
   return;
  while (l.selectedIndex !== -1)
  {
   l.removeItemAt(l.selectedIndex);
  }
  TrackPackage_history.onCloseHistory();
 },
 onCloseHistory: function()
 {
  let l = document.getElementById('historyListbox');
  let a = [];
  for (let i = 0; i < l.getRowCount(); i++)
  {
   let g = l.getItemAtIndex(i);
   let k = g.childNodes;
   let c = k.item(0).getAttribute('label');
   let n = k.item(1).getAttribute('label');
   let d = k.item(2).getAttribute('label');
   let t = k.item(3).value;
   t = t.replace(/[\;\,]+/,' ');
   if (t !== '')
    a[a.length] = c  + ',' + n  + ',' + d  + ',' + t;
   else
    a[a.length] = c  + ',' + n  + ',' + d;
  }
  let h = a.join(';');
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  p.setCharPref('TrackingHistory', h);
 },
 HistoryMapTrack: function()
 {
  let a = document.getElementById('historyListbox').selectedItems;
  for (let i = 0; i < a.length; i++)
  {
   let k = a[i].childNodes;
   let c = k.item(0).getAttribute('label');
   let n = k.item(1).getAttribute('label');
   if (a.length > 1)
    TrackPackage_functionLib.HistoryOpenMap(c, n, true);
   else
   {
    if (!TrackPackage_functionLib.GetTabSetting())
     TrackPackage_functionLib.HistoryOpenMap(c, n, false);
    else
     TrackPackage_functionLib.HistoryOpenMap(c, n, true);
   }
  }
 },
 CopyNumber: function()
 {
  let h = Components.classes['@mozilla.org/widget/clipboardhelper;1'].getService(Components.interfaces.nsIClipboardHelper);
  let a = document.getElementById('historyListbox').selectedItems;
  if (a.length === 1)
  {
   let s = a[0].childNodes.item(1).getAttribute('label');
   h.copyString(s);
  }
  else
  {
   let r = '';
   for (let i = 0; i < a.length; i++)
   {
    let k = a[i].childNodes;
    let c = k.item(0).getAttribute('label');
    let n = k.item(1).getAttribute('label');
    r += c + ': ' + n + '\n';
   }
   h.copyString(r.trim());
  }
 },
 AddToHistory: function()
 {
  let n = prompt('Enter your Tracking Number:', '');
  if (n === '' || n === null)
   return;
  let c = TrackPackage_functionLib.GetPackageCarrier(n);
  TrackPackage_functionLib._AddToHistory(c, n);
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  let s = '';
  if (p.prefHasUserValue('TrackingHistory'))
   s = p.getCharPref('TrackingHistory');
  else if (p.prefHasUserValue('tpTrackingHistory'))
   s = p.getCharPref('tpTrackingHistory');
  let a = TrackPackage_history._BuildHistoryArray(s);
  TrackPackage_history._PopulateListBox(a);
 },
 ClearHistory: function()
 {
  let p = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.');
  if (p.prefHasUserValue('TrackingHistory'))
  {
   if (confirm('Are you sure you want to clear your Tracking History?'))
   {
    p.setCharPref('TrackingHistory', '');
    let b = document.getElementById('historyListbox');
    let c = b.getRowCount();
    for (let i = 0; i < c; i++)
    {
     b.removeItemAt(0);
    }
    TrackPackage_history.onCloseHistory();
   }
  }
 },
 OpenHistoryOptions: function()
 {
  let w = window.open('chrome://trackpackage/content/prefDialog.xul', 'prefdialog', 'chrome,screenX=150,screenY=150');
  w.focus();
  w.onunload = TrackPackage_history.HistoryInit;
 }
};
window.addEventListener('load', TrackPackage_history.HistoryInit, false);
