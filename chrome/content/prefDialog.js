// Copyright Dave Kahler. Do not copy without permission.
var TrackPackage_prefs =
{
 _Prefs: Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefService).getBranch('extensions.trackpackage.'),
 _observerService: Components.classes['@mozilla.org/observer-service;1'].getService(Components.interfaces.nsIObserverService),
 _XMLObject: null,
 _LoadCheckSetting: function(pref, def)
 {
  if (TrackPackage_prefs._Prefs.prefHasUserValue(pref))
   document.getElementById(pref).setAttribute('checked', TrackPackage_prefs._Prefs.getBoolPref(pref));
  else if (TrackPackage_prefs._Prefs.prefHasUserValue('tp' + pref))
   document.getElementById(pref).setAttribute('checked', TrackPackage_prefs._Prefs.getBoolPref('tp' + pref));
  else
   document.getElementById(pref).setAttribute('checked', def);
 },
 _LoadIntValueSetting: function(pref, def)
 {
  if (TrackPackage_prefs._Prefs.prefHasUserValue(pref))
   document.getElementById(pref).value = TrackPackage_prefs._Prefs.getIntPref(pref);
  else if (TrackPackage_prefs._Prefs.prefHasUserValue('tp' + pref))
   document.getElementById(pref).value = TrackPackage_prefs._Prefs.getIntPref('tp' + pref);
  else
   document.getElementById(pref).value = def;
 },
 _LoadCharValueSetting: function(pref, def)
 {
  if (TrackPackage_prefs._Prefs.prefHasUserValue(pref))
   document.getElementById(pref).setAttribute('value', TrackPackage_prefs._Prefs.getCharPref(pref));
  else if (TrackPackage_prefs._Prefs.prefHasUserValue('tp' + pref))
   document.getElementById(pref).setAttribute('value', TrackPackage_prefs._Prefs.getCharPref('tp' + pref));
  else
   document.getElementById(pref).setAttribute('value', def);
 },
 LoadSettings: function()
 {
  TrackPackage_prefs._LoadCheckSetting('NewTab', true);
  TrackPackage_prefs._LoadCheckSetting('SmartSense', true);
  TrackPackage_prefs._LoadCheckSetting('Notifications', true);
  TrackPackage_prefs._LoadCheckSetting('PrivateBrowsing', true);
  TrackPackage_prefs._LoadIntValueSetting('MaxNumbers', 25);
  TrackPackage_prefs._LoadIntValueSetting('MaxDropdownItems', 5);
  TrackPackage_prefs._LoadCheckSetting('EnableGMaps', false);
  TrackPackage_prefs._LoadCharValueSetting('UpdateURL', 'https://realityripple.com/Software/XUL/Track-Package/defaults.xml');
  let a = TrackPackage_functionLib.regexDefaults.split(';');
  if (TrackPackage_prefs._Prefs.prefHasUserValue('Regex'))
   a = TrackPackage_prefs._Prefs.getCharPref('Regex').split(';');
  let o = document.getElementById('regexListbox');
  for (let i = 0; i < a.length; i++)
  {
   let carrierRegex = TrackPackage_functionLib.ExtractQuotedStrings(a[i]);
   if (carrierRegex === '')
    return;
   let r = document.createElement('listitem');
   let c = document.createElement('textbox');
   let n = document.createElement('textbox');
   r.setAttribute('allowevents', true);
   c.setAttribute('value', carrierRegex[0]);
   n.setAttribute('value', carrierRegex[1]);
   r.appendChild(c);
   r.appendChild(n);
   o.appendChild(r);
  }
  let d = TrackPackage_functionLib.URLDefaults.split(';');
  if (TrackPackage_prefs._Prefs.prefHasUserValue('URL'))
   d = TrackPackage_prefs._Prefs.getCharPref('URL').split(';');
  else if (TrackPackage_prefs._Prefs.prefHasUserValue('tpURL'))
   d = TrackPackage_prefs._Prefs.getCharPref('tpURL').split(';');
  let b = document.getElementById('urlListbox');
  for (let i = 0; i < d.length; i++)
  {
   let u = TrackPackage_functionLib.ExtractQuotedStrings(d[i]);
   if (u === '')
    continue;
   let r = document.createElement('listitem');
   let c = document.createElement('textbox');
   let c1 = document.createElement('textbox');
   let c2  = document.createElement('textbox');
   r.setAttribute('allowevents',true);
   c.setAttribute('value', u[0]);
   c1.setAttribute('value', u[1]);
   c2.setAttribute('value', u[2]);
   r.appendChild(c);
   r.appendChild(c1);
   r.appendChild(c2);
   b.appendChild(r);
  }
  return true;
 },
 SaveSettings: function()
 {
  TrackPackage_prefs._Prefs.setBoolPref('NewTab', document.getElementById('NewTab').checked);
  TrackPackage_prefs._Prefs.setBoolPref('SmartSense', document.getElementById('SmartSense').checked);
  TrackPackage_prefs._Prefs.setBoolPref('Notifications', document.getElementById('Notifications').checked);
  TrackPackage_prefs._Prefs.setBoolPref('PrivateBrowsing', document.getElementById('PrivateBrowsing').checked);
  TrackPackage_prefs._Prefs.setIntPref('MaxNumbers', document.getElementById('MaxNumbers').value);
  TrackPackage_prefs._Prefs.setIntPref('MaxDropdownItems', document.getElementById('MaxDropdownItems').value);
  TrackPackage_prefs._Prefs.setBoolPref('EnableGMaps', document.getElementById('EnableGMaps').checked);
  TrackPackage_prefs._Prefs.setCharPref('UpdateURL', document.getElementById('UpdateURL').value);
  let o = document.getElementById('regexListbox');
  let a = [];
  let c = [];
  let n = o.getRowCount();
  for (let i = 0; i < n; i++)
  {
   let b = o.getItemAtIndex(i);
   let d = b.childNodes;
   let s = d.item(0).value;
   if (typeof s === 'undefined' || s === null)
    s = d.item(0).getAttribute('value');
   let r = d.item(1).value;  
   if (typeof r === 'undefined' || r === null)
    r = d.item(1).getAttribute('value');
   a[a.length] = '"' + s + '"' + ',' + '"' + r + '"';
   c[c.length] = s;
  }
  let f = a.join(';');
  let u = document.getElementById('urlListbox');
  let y = [];
  let w = [];
  let x = u.getRowCount();
  for (let i = 0; i < x; i++)
  {
   let b = u.getItemAtIndex(i);
   let d = b.childNodes;
   let r = d.item(0).value;
   if (typeof r === 'undefined' || r === null)
    r = d.item(0).getAttribute('value');
   let u1 = d.item(1).value;
   if (typeof u1 === 'undefined' || u1 === null)
    u1 = d.item(1).getAttribute('value');
   let u2 = d.item(2).value;
   if (typeof u2 === 'undefined' || u2 === null)
    u2 = d.item(2).getAttribute('value');
   if (typeof r === 'undefined' || r === null)
    continue;
   y[y.length] = '"' + r + '"' + ',' + '"' + u1 + '"' + ',' + '"' + u2 + '"';
   w[w.length] = r;
  }
  let urlPref = y.join(';');
  n = c.length;
  x = w.length;
  for (let i = 0; i < n; i++)
  {
   if (typeof c[i] === 'undefined' || c[i] === null || c[i] === '')
    continue;
   let foundMatch = false;
   for (let j = 0; j < x; j++)
   {
    if (typeof w[j] === 'undefined' || w[j] === null || w[j] === '')
     continue;
    if (c[i].toLowerCase() === w[j].toLowerCase())
    {
     foundMatch = true;
     break;
    }
   }
   if (!foundMatch)
   {
    alert('Please make sure an entry for Carrier \'' + c[i] + '\' is included in the Service URLs list!\nIf a carrier is listed in the Tracking Number Detections list, it must have a valid Service URL.');
    return false;
   }
  }
  TrackPackage_prefs._Prefs.setCharPref('URL', urlPref);
  TrackPackage_prefs._Prefs.setCharPref('Regex', f);
  TrackPackage_prefs._observerService.notifyObservers(null, 'trackpackage-settings', 'changed');
  return true;
 },
 AddNewRegex: function()
 {
  let b = document.getElementById('regexListbox');
  let r = document.createElement('listitem');
  let c = document.createElement('textbox');
  let d = document.createElement('textbox');
  r.setAttribute('allowevents', true);
  c.setAttribute('value', '');
  d.setAttribute('value', '');
  r.appendChild(c);
  r.appendChild(d);
  b.appendChild(r);
 },
 DeleteRegex: function()
 {
  let b = document.getElementById('regexListbox');
  b.removeItemAt(b.selectedIndex);
 },
 AddNewURL: function()
 {
  let b = document.getElementById('urlListbox');
  let r = document.createElement('listitem');
  let c = document.createElement('textbox');
  let u1 = document.createElement('textbox');
  let u2 = document.createElement('textbox');
  r.setAttribute('allowevents', true);
  c.setAttribute('value', '');
  u1.setAttribute('value', '');
  u2.setAttribute('value', '');
  r.appendChild(c);
  r.appendChild(u1);
  r.appendChild(u2);
  b.appendChild(r);
 },
 DeleteURL: function()
 {
  let b = document.getElementById('urlListbox');
  b.removeItemAt(b.selectedIndex);
 },
 ResetRegexDefaults: function()
 {
  if(!confirm('Are you sure you want to reset the Tracking Number Detections to default? This cannot be undone!'))
   return;
  TrackPackage_prefs._Prefs.setCharPref('Regex', TrackPackage_functionLib.regexDefaults);
  let b = document.getElementById('regexListbox');
  let x = b.getRowCount();
  for (let i = 0; i < x; i++)
  {
   b.removeItemAt(0);
  }
  let a = TrackPackage_functionLib.regexDefaults.split(';');
  for (let i = 0; i < a.length; i++)
  {
   let c = TrackPackage_functionLib.ExtractQuotedStrings(a[i]);
   let r = document.createElement('listitem');
   let e = document.createElement('textbox');
   let d = document.createElement('textbox');
   r.setAttribute('allowevents', true);
   e.setAttribute('value', c[0]);
   d.setAttribute('value', c[1]);
   r.appendChild(e);
   r.appendChild(d);
   b.appendChild(r);
  }
 },
 ResetURLDefaults: function()
 {
  if(!confirm('Are you sure you want to reset the Service URLs to default? This cannot be undone!'))
   return;
  TrackPackage_prefs._Prefs.setCharPref('URL', TrackPackage_functionLib.URLDefaults);
  let b = document.getElementById('urlListbox');
  let x = b.getRowCount();
  for (let i = 0; i < x; i++)
  {
   b.removeItemAt(0);
  }
  let a = TrackPackage_functionLib.URLDefaults.split(';');
  for (let i = 0; i < a.length; i++)
  {
   let c = TrackPackage_functionLib.ExtractQuotedStrings(a[i]);
   let r = document.createElement('listitem');
   let e = document.createElement('textbox');
   let u1 = document.createElement('textbox');
   let u2 = document.createElement('textbox');
   r.setAttribute('allowevents', true);
   e.setAttribute('value', c[0]);
   u1.setAttribute('value', c[1]);
   u2.setAttribute('value', c[2]);
   r.appendChild(e);
   r.appendChild(u1);
   r.appendChild(u2);
   b.appendChild(r);
  }
 },
 SaveXML: function()
 {
  let p = Components.classes['@mozilla.org/filepicker;1'].createInstance(Components.interfaces.nsIFilePicker);
  p.init(window, 'Save As', p.modeSave);
  p.appendFilters(Components.interfaces.nsIFilePicker.filterXML);
  p.defaultExtension = 'xml';
  p.defaultString = 'customXML';
  let r = p.show();
  if (r !== p.returnOK && r !== p.returnReplace)
   return;
  let x = '<?xml version="1.0"?>\n\n';
  x += '<trackpackage>\n\n';
  let a = TrackPackage_functionLib.GetRegexURLArray();
  for (let i = 0; i < a.length; i++)
  {
   if (a[i][1] === '')
    continue;
   x += '\t<regex carrier="' + a[i][0] + '" value="' + a[i][1] + '" />\n';
  }
  x += '\n';
  for (let i = 0; i < a.length; i++)
  {
   x += '\t<url carrier="' + a[i][0] + '" front="' + a[i][2] + '" back="' + a[i][3] + '" />\n';
  }
  x += '\n';
  x += '</trackpackage>\n';
  let g = new RegExp('&','gi');
  x = x.replace(g,'&amp;');
  let o = Components.classes['@mozilla.org/network/file-output-stream;1'].createInstance(Components.interfaces.nsIFileOutputStream);
  let f = Components.classes['@mozilla.org/file/local;1'].createInstance(Components.interfaces.nsIFile);
  f.initWithPath(p.file.path);
  o.init(f, 0x02 | 0x08 | 0x20, 0664, 0);
  o.write(x, x.length);
  o.close();
 },
 _xmlLoadError: function()
 {
  alert('Error loading XML file. Check the URL or try again later.');
 },
 processXML: function(xmlDoc)
 {
  if (!TrackPackage_prefs._XMLObject.responseXML)
  {
   TrackPackage_prefs._xmlLoadError();
   return;
  }
  let r = TrackPackage_prefs._XMLObject.responseXML.firstChild;
  if (!r)
  {
   TrackPackage_prefs._xmlLoadError();
   return;
  }
  if (r.nodeName === 'parsererror')
  {
   TrackPackage_prefs._xmlLoadError();
   return;
  }
  let n = r.childNodes;
  let a = [];
  let u = [];
  for (let i = 0; i < n.length; i++)
  {
   let k = n.item(i);
   let s = k.nodeName;
   if (s === 'regex')
   {
    let c = k.getAttribute('carrier');
    let v = k.getAttribute('value');
    a[a.length] = '"' + c + '","' + v + '"';
   }
   if (s === 'url')
   {
    let c = k.getAttribute('carrier');
    let v1 = k.getAttribute('front');
    let v2 = k.getAttribute('back');
    u[u.length] = '"' + c + '","' + v1 + '","' + v2 + '"';
   }
  }
  let g = a.join(';');
  let l = u.join(';');
  TrackPackage_prefs._Prefs.setCharPref('Regex', g);
  TrackPackage_prefs._Prefs.setCharPref('URL', l);
  let b = document.getElementById('regexListbox');
  let x = b.getRowCount();
  for (let i = 0; i < x; i++)
  {
   b.removeItemAt(0);
  }
  let o = document.getElementById('urlListbox');
  x = o.getRowCount();
  for (let i = 0; i < x; i++)
  {
   o.removeItemAt(0);
  }
  TrackPackage_prefs.LoadSettings();
  alert('Successfully loaded remote XML preferences!');
 },
 UpdateDefs: function()
 {
  if(!confirm('Are you sure you want to update definitions from the Internet?\nThis will overwrite all your current Tracking Number Definitions and Service URLs and cannot be undone.'))
   return;
  let d = document.implementation.createDocument('', '', null);
  d.addEventListener('load', function(event){TrackPackage_prefs.processXML(event.currentTarget);}, false);
  let r = new XMLHttpRequest();
  TrackPackage_prefs._XMLObject = r;
  r.open('GET', document.getElementById('UpdateURL').value, true);
  r.channel.loadFlags |= Components.interfaces.nsIRequest.LOAD_BYPASS_CACHE;
  r.onload = TrackPackage_prefs.processXML;
  r.onerror = TrackPackage_prefs._xmlLoadError;
  r.send(null);
 }
};
